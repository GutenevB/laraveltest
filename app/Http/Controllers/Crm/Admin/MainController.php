<?php

namespace App\Http\Controllers\Crm\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends AdminBaseController
{
    function index()
    {
        return view('crm.admin.index');
    }
}

@extends('layouts.admin')

@section('content')

    <a href="{{ route('crm\admin.user.create') }}" class="btn btn-outline-primary"> Создать заказ </a>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Email</td>
                <td>Дата создания</td>
                <td>Действия</td>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>
                        <a href="{{ route('crm\admin.user.edit',$user->id) }}"><i class="fa fa-fw fa-eye"></i></a>
                        <form action="{{ route('crm\admin.user.destroy',['id'=>$user->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-outline-danger" value="Удалить">
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>



@endsection

<?php

namespace App\Http\Controllers\Crm\Admin;

use App\Http\Controllers\Crm\BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

abstract class AdminBaseController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('status');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>['status','auth']], function (){
    $groupData = [
        'namespace'=> 'Crm\Admin',
        'prefix' => 'admin'
    ];
    Route::group($groupData,function (){
        Route::resource('index','MainController')->names('crm.admin.index');
    });
    Route::group($groupData,function (){
        Route::resource('brand','BrandController')->names('crm\admin.brand');
                Route::post('brand/create','BrandController@store');
                Route::post('brand/{id}','BrandController@update');
                Route::get('brand/add/{id}/{id_user}','BrandController@add')->name('crm\admin.brand.add');
    });
    Route::group($groupData,function (){
        Route::resource('user','UserController')->names('crm\admin.user');
        Route::post('user/create','UserController@store');
        Route::post('user/{id}','UserController@update');
    });
});


<?php

namespace App\Http\Controllers\Crm\Admin;

use App\Models\Admin\Brands;
use App\Models\Admin\User;
use Illuminate\Http\Request;
use App\Repositories\Admin\BrandRepository;

class BrandController extends AdminBaseController
{
    protected $brandRepository;

    public function __construct()
    {
        parent::__construct();
        $this->brandRepository = app(BrandRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perpage = 5;
        $brands = Brands::all();
        return view('crm.admin.brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm.admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:50',
            'descr' => 'required|max:255',
        ]);

        $brand = new Brands();
        $brand->name = $request->title;
        $brand->descr = $request->descr;
        $save = $brand->save();
        $arr = array('message' => 'Что-то идет не так. Пожалуйста, попробуйте позже', 'status' => false);
        if ($save) {
            $arr = array('message' => 'Отлично, уже все в базе!', 'status' => true);
        }
        return Response()->json($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('here1 show');
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = $this->brandRepository->getAllUserBrand($id);
        $userFree = $this->brandRepository->getFreeUser();

        return view('crm.admin.brand.edit', compact('brand', 'userFree'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:50',
            'descr' => 'required|max:255',
        ]);
        $brand = Brands::find($id);
        $brand->name = $request->title;
        $brand->descr = $request->descr;
        $save = $brand->update();

        $arr = array('message' => 'Что-то идет не так. Пожалуйста, попробуйте позже', 'status' => false);
        if ($save) {
            $arr = array('message' => 'Отлично, бновление прошло успешно', 'status' => true);
        }
        return Response()->json($arr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brands::find($id);
        $brand->delete();
        return redirect()->route('crm\admin.brand.index');
    }

    /**
     * @param $id
     * @param $user_id
     */
    public function add($id, $user_id)
    {
        $user = User::find($user_id);
        $user->brands()->associate($id);
        $user->save();
        return redirect()->route('crm\admin.brand.edit',[$id]);
    }
}

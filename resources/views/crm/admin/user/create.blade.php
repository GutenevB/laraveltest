@extends('layouts.admin')

@section('content')
    <div class="col-md-6 offset-3">
        <form action="" id="create_new_user" method="post">
            @csrf
            <h3>Новый клиент</h3>

            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control" required placeholder="Name" value="">
            </div>
            <div class="form-group">
                <input name="email" id="email" class="form-control" placeholder="Email" value="">
            </div>
            <div class="alert alert-success d-none" id="msg_div">
                <span id="res_message"></span>
            </div>

            <input type="submit" class="btn-outline-primary btn" value="Создать">

        </form>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            $('#create_new_user').on('submit', function (e) {
                e.preventDefault();
                var name = $('#name').val();
                var email = $('#email').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('admin/user')}}",
                    data: {"_token": "{{ csrf_token() }}", name: name, email: email},
                    success: function (response) {
                        if(response.status == true) {
                            $('#send_form').html('Submit');
                            $('#res_message').show();
                            $('#res_message').html(response.message);
                            $('#msg_div').removeClass('d-none');
                            setTimeout(function () {
                                $('#res_message').hide();
                                $('#msg_div').hide();
                            }, 10000);
                        }
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        $('#res_message').show();
                        $('#res_message').html(errors.message);
                        $('#msg_div').removeClass('d-none');
                    }
                });
            });
        });

    </script>
@endsection



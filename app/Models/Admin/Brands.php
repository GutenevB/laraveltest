<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;


class Brands extends Model
{
    protected $fillable=[
        'name',
        'descr'
    ];

    public function users()
    {
        return $this->hasMany(\App\User::class, 'brand_id', 'id');
    }

}

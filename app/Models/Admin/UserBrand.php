<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class UserBrand extends Model
{
    protected $fillable= [
        'user_id',
        'brand_id'
    ];
    public $timestamps = false;
}

@extends('layouts.admin')

@section('content')

    <a href="{{ route('crm\admin.brand.create') }}" class="btn btn-outline-primary"> Создать заказ </a>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td>Id</td>
                                <td>Name</td>
                                <td>Дата создания</td>
                                <td>Действия</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($brands as $brand)
                            <tr>
                                <td>{{ $brand->id }}</td>
                                <td>{{ $brand->name }}</td>
                                <td>{{ $brand->created_at }}</td>
                                <td>
                                    <a href="{{ route('crm\admin.brand.edit',$brand->id) }}"><i class="fa fa-fw fa-eye"></i></a>
                                    <form action="{{ route('crm\admin.brand.destroy',['id'=>$brand->id]) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn btn-outline-danger" value="Удалить">
                                    </form>

                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>



@endsection

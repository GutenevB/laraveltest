<?php

use Illuminate\Database\Seeder;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name'=>'admin'],
            ['name'=>'client']
        ];
        DB::table('roles')->insert($data);
    }
}

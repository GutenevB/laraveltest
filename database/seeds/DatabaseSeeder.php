<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Admin\Brands::class,7)->create();
        $this->call(Roles::class);
        factory(\App\Models\UserRole::class, 25)->create();

    }
}

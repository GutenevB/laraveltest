<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin\Brands;
use Faker\Generator as Faker;

$factory->define(Brands::class, function (Faker $faker) {
    $descr = $faker->realText(rand(10,40));
    return [
        'name'=>$faker->company,
        'descr'=>$descr
    ];
});

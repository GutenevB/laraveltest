@extends('layouts.admin')

@section('content')
    @foreach($brand as $brandInfo)
    <div class="col-md-6">
        <form action="" id="create_brand" method="post">
            @method('PUT')
            @csrf
            <h3>Редактировать компанию</h3>

            <div class="form-group">
                <input type="text" name="title" id="title" class="form-control" required value="{{ $brandInfo->name }}">
            </div>
            <div class="form-group">
                <textarea name="descr" id="descr" class="form-control" rows="10">{{ $brandInfo->descr }}</textarea>
            </div>
            <div class="alert alert-success d-none" id="msg_div">
                <span id="res_message"></span>
            </div>
            <input type="hidden" id="hidden_id" value="{{ $brandInfo->id }}">
            <input type="submit" class="btn-outline-primary btn" value="Изменить">
        </form>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <td>Id</td>
                        <td>Name</td>
                        <td>Дата создания</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($brandInfo->users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
            </table>
        </div>
    </div>
        <div class="col-md-6 offset-3">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <td>Id</td>
                    <td>Name</td>
                    <td>Действие</td>
                </tr>
                </thead>
                <tbody>
                @foreach($userFree as $uf)
                    <tr>
                        <td>{{ $uf->id }}</td>
                        <td>{{ $uf->name }}</td>
                        <td><a href="{{ route('crm\admin.brand.add',[$brandInfo->id,$uf->id]) }}">Добавить в компанию</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endforeach
    <script>
        jQuery(document).ready(function ($) {
            $('#create_brand').on('submit', function (e) {
                e.preventDefault();
                var title = $('#title').val(),
                    descr = $('#descr').val(),
                    id = $('#hidden_id').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('admin/brand')}}"+"/"+ id,
                    data: {"_token": "{{ csrf_token() }}", title: title, descr: descr},
                    success: function (response) {
                        $('#send_form').html('Submit');
                        $('#res_message').show();
                        $('#res_message').html(response.message);
                        $('#msg_div').removeClass('d-none');
                        document.getElementById("create_brand").reset();
                        setTimeout(function () {
                            $('#res_message').hide();
                            $('#msg_div').hide();
                        }, 10000);
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        $('#res_message').show();
                        $('#res_message').html(errors.message);
                        $('#msg_div').removeClass('d-none');
                    }
                });
            });
        });

    </script>
@endsection



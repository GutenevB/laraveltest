<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserRole;
use App\Models\Admin\Brands;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;

$factory->define(UserRole::class, function (Faker $faker) {
    $role = 1;
    $user = factory(\App\User::class)->create()->id;

    if($user != 1){
        $role = '2';
    }

    return [
        'user_id'=>(string)$user,
        'role_id'=> $role
    ];
});

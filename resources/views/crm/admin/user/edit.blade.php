@extends('layouts.admin')

@section('content')
    <div class="col-md-6 offset-3">
        <form action="" id="create_user" method="post">
            @method('PUT')
            @csrf
            <h3>Редактировать компанию</h3>

            <div class="form-group">
                <input type="text" name="title" id="name" class="form-control" required value="{{ $user->name }}">
            </div>
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control" required value="{{ $user->email }}">
            </div>
            <div class="alert alert-success d-none" id="msg_div">
                <span id="res_message"></span>
            </div>
            <input type="hidden" id="hidden_id" value="{{ $user->id }}">
            <input type="submit" class="btn-outline-primary btn" value="Изменить">

        </form>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            $('#create_user').on('submit', function (e) {
                e.preventDefault();
                var name = $('#name').val(),
                    email = $('#email').val(),
                    id = $('#hidden_id').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('admin/user')}}"+"/"+ id,
                    data: {"_token": "{{ csrf_token() }}", name: name, email: email},
                    success: function (response) {
                        $('#send_form').html('Submit');
                        $('#res_message').show();
                        $('#res_message').html(response.message);
                        $('#msg_div').removeClass('d-none');
                        document.getElementById("create_user").reset();
                        setTimeout(function () {
                            $('#res_message').hide();
                            $('#msg_div').hide();
                        }, 10000);
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        $('#res_message').show();
                        $('#res_message').html(errors.message);
                        $('#msg_div').removeClass('d-none');
                    }
                });
            });
        });

    </script>
@endsection



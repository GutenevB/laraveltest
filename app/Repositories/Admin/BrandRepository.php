<?php


namespace App\Repositories\Admin;


use App\Repositories\CoreRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Brands as Models;
use App\Models\Admin\User as User;

class BrandRepository extends CoreRepository
{
    public $model;

    public function __construct()
    {
        parent::__construct();
    }

    protected function getModelClass()
    {
        return Models::class;
    }


    public function getAllUserBrand($id)
    {
        return $this->startConditions()->with(['users'])->where('id', $id)->get();

    }

    public function getFreeUser(){
        return  User::where('brand_id',null)->get();
    }






}

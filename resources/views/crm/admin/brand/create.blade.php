@extends('layouts.admin')

@section('content')
    <div class="col-md-6 offset-3">
        <form action="" id="create_brand" method="post">
            @csrf
            <h3>Создать компанию</h3>

            <div class="form-group">
                <input type="text" name="title" id="title" class="form-control" required value="">
            </div>
            <div class="form-group">
                <textarea name="descr" id="descr" class="form-control" rows="10"></textarea>
            </div>
            <div class="alert alert-success d-none" id="msg_div">
                <span id="res_message"></span>
            </div>

            <input type="submit" class="btn-outline-primary btn" value="Создать">

        </form>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            $('#create_brand').on('submit', function (e) {
                e.preventDefault();
                var title = $('#title').val();
                var descr = $('#descr').val();

                $.ajax({
                    type: "POST",
                    url: "{{url('admin/brand')}}",
                    data: {"_token": "{{ csrf_token() }}", title: title, descr: descr},
                    success: function (response) {
                            $('#send_form').html('Submit');
                            $('#res_message').show();
                            $('#res_message').html(response.message);
                            $('#msg_div').removeClass('d-none');
                            document.getElementById("create_brand").reset();
                            setTimeout(function () {
                                $('#res_message').hide();
                                $('#msg_div').hide();
                            }, 10000);
                    },
                    error: function (data) {
                        var errors = data.responseJSON;
                        $('#res_message').show();
                        $('#res_message').html(errors.message);
                        $('#msg_div').removeClass('d-none');
                    }
                });
            });
        });

    </script>
@endsection



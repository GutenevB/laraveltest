<?php

namespace App\Http\Controllers\Crm\Admin;

use App\Models\Admin\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('crm.admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm.admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'email' => 'required|email'
        ]);
        $arr = '';
        $find_old_user = User::where('email', $request->email)->count();
        if ($find_old_user > 0) {
            $arr = array('message' => 'Такая почтка у нас уже есть', 'status' => false);
        } else {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
            $save = $user->save();
            $arr = array('message' => 'Что-то идет не так. Пожалуйста, попробуйте позже', 'status' => false);
            if ($save) {
                $arr = array('message' => 'Отлично, уже все в базе!', 'status' => true);
            }
        }
        return Response()->json($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('crm.admin.user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'email' => 'required|email'
        ]);
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $save = $user->update();

        $arr = array('message' => 'Что-то идет не так. Пожалуйста, попробуйте позже', 'status' => false);
        if($save){
            $arr = array('message' => 'Отлично, бновление прошло успешно', 'status' => true);
        }
        return Response()->json($arr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('crm\admin.user.index');
    }
}
